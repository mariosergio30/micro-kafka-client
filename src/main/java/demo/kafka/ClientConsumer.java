package demo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import demo.Application;

@Component
public class ClientConsumer {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);


  // call on browser localhost:8003/producer?topic=pagamento-aprovado&message=mensagemX
  @KafkaListener(topics = "${app.kafka.consumer.topics}", autoStartup = "${app.kafka.consumer.enabled}")
  public void onMessage(ConsumerRecord<String, String> record) {
    
	  LOG.info("DEMO: CONSUMER Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
	 	 
  }


}

