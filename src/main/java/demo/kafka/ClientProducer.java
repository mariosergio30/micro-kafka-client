package demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import demo.Application;

@Component
public class ClientProducer  {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);


	@Autowired
	private KafkaTemplate<String, String> templateProducer;

	public void send(String topic, String message) {

		templateProducer.send(topic, message);

		LOG.info("DEMO: Sent method Finalized [{}] to topic [{}]", message, topic);
	}



}
