package demo.webapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.kafka.ClientProducer;



@RestController 
public class Controler {

	@Autowired 
	private ClientProducer clientProducer;
	
	@RequestMapping(value="/", method = RequestMethod.GET)	
	public ResponseEntity<String> root() {
			
		return new ResponseEntity<String>("<hr>Kafka Cliente App is ON</hr> <br><br><b>Note: use /producer?topic=order_requested&message= to publish a menssage into Kafka Queue ! </b>",HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/producer", method = RequestMethod.GET)	
	public ResponseEntity<String> producer(@RequestParam String topic, @RequestParam String message) {
		
		if (message == null || message.equals(""))
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
				
		try {
		
			clientProducer.send(topic,message);
			
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),HttpStatus.INTERNAL_SERVER_ERROR);
		} 
					
		
		// SUCCESS
		return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(),HttpStatus.OK);
	}
	
	

}



